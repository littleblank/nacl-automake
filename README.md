About
=====

Simplify to write the makefile for Native Client


Development Status
==================

Planning


Installation
============

Method 1: Copy the directory - naclautomake to Python/Lib

Method 2: Install [it](https://pypi.python.org/pypi/naclautomake)  by [pypi](https://pypi.python.org)

Method 3: Run setup.py in [source code](https://bitbucket.org/alexchicn/nacl-automake/get/master.tar.gz)


Usage
=====

See [Wiki](https://bitbucket.org/alexchicn/nacl-automake/wiki)

or Visit [my webiste](http://alexchi.me/)


Example
=======

not ready for new version

Todo
====

The status to be Pre-Alpha

License
=======

Copyright 2013 Alex Chi

NaCl Automake is licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/).

Thanks
======

1. [**Bitbucket**](https://bitbucket.org/): provides a **Free** repository

2. [**JetBrain**](http://www.jetbrains.com/): provides a **Free Open Source** license of [**PyCharm**](http://www.jetbrains.com/pycharm/)
