'''
Created on 2013-2-7

@author: Alex Chi
'''
from common import EProjectType
from common import UniqueList
from makefile import NaClMakefile
from codefile import NaClCodeFile
from compiler import NaClCompiler

class NaClProject(NaClMakefile):
    '''
a project that contain all code files
    '''

    def __init__(self, name, setting, projectpath, projecttype, solution):
        self.__name__ = name
        self.__setting__ = setting
        self.__type__ = projecttype
        self.__projectpath__ = projectpath
        self.__solution__ = solution
        self.__depends__ = []
        self.__bedepends__ = []
        self.__incpaths__ = []
        self.__incs__ = []
        self.__srcs__ = []

    @property
    def name(self):
        '''
the name of project
        '''
        return self.__name__

    @property
    def type(self):
        '''
the type of project. you can refer the common.EProjectType(.a, .so or .exe).
        '''
        return self.__type__

    @property
    def depends(self):
        return self.__depends__

    def addDepend(self, value):
        self.__depends__ += value
        # rearrange the list of depends
        self.__depends__ = UniqueList(self.__depends__)

    @property
    def bedepends(self):
        return self.__bedepends__

    def addBedepend(self, value):
        self.__bedepends__ += value
        # rearrange the list of bedepends
        self.__bedepends__ = UniqueList(self.__bedepends__)

    @property
    def incPaths(self):
        return self.__incpaths__

    def addIncPath(self, path):
        self.__incpaths__.append(path)

    @property
    def incs(self):
        return self.__incs__

    @property
    def srcs(self):
        return self.__srcs__

    def addIncFile(self, codefile):
        self.__incs__.append(codefile)

    def addSrcFile(self, codefile):
        self.__srcs__.append(codefile)

    def addNaClCodeFile(self, codefile):
        if codefile.isInc():
            self.addIncFile(codefile)
        elif codefile.isSrc():
            self.addSrcFile(codefile)
        else:
            raise Exception('it is not a code file.')

    def addCodeFile(self, fullname, path = ''):
        self.addNaClCodeFile(NaClCodeFile(fullname, path))

    @property
    def outputpath(self):
        return self.__setting__.outputPath + '/' + self.__name__

    def makeoutputdirKey(self, toolchain = '', maketype = ''):
        res = self.outputpath
        if 0 < len(toolchain):
            res = '/'.join([res, toolchain])
        if 0 < len(maketype):
            res = '/'.join([res, maketype])
        return res

    def makeoutputdirPath(self, toolchain = '', maketype = ''):
        res = self.outputpath
        if 0 < len(toolchain):
            res = '/'.join([res, toolchain])
        if 0 < len(maketype):
            res = '/'.join([res, maketype])
        return res

    def makeoutputdirValue(self, toolchain = '', maketype = ''):
        return '\t$(MKDIR) ' + self.makeoutputdirPath(toolchain, maketype)

    def mfMakeOutputDir(self):
        res = '# make output directory'
        for toolchain in self.__setting__.toolchains:
            res = '\n'.join([res, self.makeoutputdirKey(toolchain) + ':'])
            res = '\n'.join([res, self.makeoutputdirValue(toolchain)])
            for maketype in self.__setting__.maketypes:
                res = '\n'.join([res, self.makeoutputdirKey(toolchain, maketype) + ': | ' + self.makeoutputdirKey(toolchain)])
                res = '\n'.join([res, self.makeoutputdirValue(toolchain, maketype)])
        return res

    def mfIncludeHeaderDependencyFiles(self):
        res = '# Include header dependency files.'
        for toolchain in self.__setting__.toolchains:
            for maketype in self.__setting__.maketypes:
                res = '\n'.join([res, '-include ' + self.makeoutputdirPath(toolchain, maketype) + '/*.d'])
        return res

    def nmfKey(self, toolchain, maketype):
        res = self.makeoutputdirPath(toolchain, maketype)
        res = '/'.join([res, self.name])
        res = '.'.join([res, 'nmf'])
        return res

    def nmfValue(self, toolchain, maketype):
        res = '\t$(CREATENMF) -o $@ $^ -s ' + self.makeoutputdirPath(toolchain, maketype)
        return res

    def mfBuildWithToolChain(self, toolchain):
        res = '# build ' + self.name + ' with ' + toolchain
        for maketype in self.__setting__.maketypes:
            res = '\n'.join([res, ''])
            res = '\n'.join([res, self.mfBuildWithToolChainAndMakeType(toolchain, maketype)])
        return res

    def mfBuildWithToolChainAndMakeType(self, toolchain, maketype):
        res = '# build ' + self.name + ' with ' + toolchain + '|' + maketype
        compiler = NaClCompiler(self.makeoutputdirKey(toolchain, maketype)
            , toolchain, maketype, self.__setting__.ostypes, self.__type__)
        res = '\n'.join([res, compiler.makefile(self)])
        if self.__type__ == EProjectType.EXE:
            res = '\n'.join([res, ''])
            res = '\n'.join([res, self.mfGenerateNMF(toolchain, maketype)])
        return res

    def mfGenerateNMF(self, toolchain, maketype):
        res = '# generate nmf file'
        res = '\n'.join([res, 'ALL_TARGETS+=' + self.nmfKey(toolchain, maketype)])
        res = '\n'.join([res, self.nmfKey(toolchain, maketype) + ': | ' + self.makeoutputdirPath(toolchain, maketype)])
        res = '\n'.join([res, self.nmfValue(toolchain, maketype)])
        return res

    def mfBuild(self):
        res = '# build ' + self.name
        res = '\n'.join([res, self.__name__ + ':'])
        for dependname in self.__depends__:
            res = ' '.join([res, dependname])
        for toolchain in self.__setting__.toolchains:
            res = '\n'.join([res, ''])
            res = '\n'.join([res, self.mfBuildWithToolChain(toolchain)])
        return res

    def mfClean(self):
        res = 'CLEAN_TARGETS+=' + self.__name__ + '-clean'
        res = '\n'.join([res, '.PHONY: ' + self.__name__ + '-clean'])
        res = '\n'.join([res, self.__name__ + '-clean:'])
        for toolchain in self.__setting__.toolchains:
            for maketype in self.__setting__.maketypes:
                res = '\n'.join([res, '\t$(RM) -fr ' + self.outputpath + '/' + toolchain + '/' + maketype])
            res = '\n'.join([res, '\t$(RM) -fr ' + self.outputpath + '/' + toolchain])
        return res

    def makefile(self):
        res = '# Project: ' + self.__name__ + ' | Type: ' + self.__type__
        res = '\n'.join([res, self.mfMakeOutputDir()])
        res = '\n'.join([res, ''])
        res = '\n'.join([res, self.mfIncludeHeaderDependencyFiles()])
        res = '\n'.join([res, ''])
        res = '\n'.join([res, self.mfBuild()])
        res = '\n'.join([res, ''])
        res = '\n'.join([res, self.mfClean()])
        return res