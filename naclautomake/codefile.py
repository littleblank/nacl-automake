from common import ECodeFileType
from common import SplitFullName
from common import GetCodeFileType

__author__ = 'alex'

class NaClCodeFile(object):
    def __init__(self, fullname, path = ''):
        splitres = SplitFullName(fullname)
        self.__name__ = splitres[0]
        self.__ext__ = splitres[1]
        self.__path__ = path
        self.__filetype__ = GetCodeFileType(self.__ext__)
        if not self.isInc() and not self.isSrc():
            raise Exception(fullname + ' is not a code file.')

    @property
    def name(self):
        return self.__name__

    @property
    def ext(self):
        return self.__ext__

    @property
    def path(self):
        return self.__path__

    @property
    def fullname(self):
        return self.__path__ + '/' + self.__name__ + '.' + self.__ext__

    @property
    def filetype(self):
        return self.__filetype__

    def isInc(self):
        if self.__filetype__ is ECodeFileType.INC:
            return True
        return False

    def isSrc(self):
        if self.__filetype__ is ECodeFileType.SRC:
            return True
        return False