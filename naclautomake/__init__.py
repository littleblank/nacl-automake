"""
Copyright (C) 2013 Alex Chi (The MIT License)
Welcom to use nacl-automake.

Author: Alex Chi
Repo: https://bitbucket.org/alexchicn/nacl-automake
eMail: alex@alexchi.me
License: please read the LICENSE file in root directory
"""
from main import NaClAutomake

__all__ = ['NaClAutomake']
__version__ = '0.0.1'
__build__ = '20130124'
__author__ = 'Alex Chi <alex@alexchi.me>'
__license__ = 'Creative Commons Attribution 3.0 Unported License'

__all__ += ['CreateAutomake']

def CreateAutomake(solutionname, solutionpath, outputpath, naclsdkpath, chromepath, toolchains, ostypes, maketypes):
    return NaClAutomake(solutionname, solutionpath, outputpath, naclsdkpath, chromepath, toolchains, ostypes, maketypes)